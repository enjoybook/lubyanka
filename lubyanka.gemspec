# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'lubyanka/version'

Gem::Specification.new do |spec|
  spec.name          = "lubyanka"
  spec.version       = Lubyanka::VERSION
  spec.authors       = ["Alexander Marchenko"]
  spec.email         = ["axvmindaweb@gmail.com"]

  spec.summary       = %q{ CD workflow gem }
  spec.description   = %q{ CD workflow gem }
  spec.homepage      = "https://google.com"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_dependency "activesupport"
  spec.add_dependency "scaleway"
  spec.add_dependency "git"
  spec.add_dependency "knife-solo"
  spec.add_dependency "knife-solo_data_bag", '~> 2.1'
  spec.add_dependency "berkshelf"

  spec.add_dependency 'cloudflair'
  spec.add_dependency 'faraday', '>=0.13', '<0.14'

  spec.add_dependency 'capistrano', '~> 3.6'
  spec.add_dependency 'capistrano-rails', '~> 1.3'
  spec.add_dependency 'capistrano-rbenv', '~> 2.0'
  spec.add_dependency 'capistrano3-puma', '~> 3.1'
end
