module Lubyanka
  module SoftwareManager
    class Engine
      include Lubyanka::Helpers::Logger
      attr_reader :role

      def initialize
        @role = :app
      end

      def configure(server)
        prepare_directory
        install(server)
      end

      def install(server)
        cmd "cd .lubyanka && knife solo bootstrap root@#{server.public_ip} nodes/#{role}.json"
      end

      def finish(server)
        cmd "cd .lubyanka && knife solo clean root@#{server.public_ip} nodes/#{role}.json"
      end

      private

      def dir_path
        File.join(Dir.pwd, 'config', 'lubyanka', 'gulag', role.to_s)
      end

      def prepare_directory
        cmd 'rm -rf .lubyanka'
        cmd 'knife solo init .lubyanka'

        knife_local_mode true
        render_berksfile

        # cmd "rm -rf .lubyanka/data_bags && ln -s #{dir_path}/data_bags .lubyanka/data_bags"
        render_databags

        cmd "mkdir -p .lubyanka/site-cookbooks/#{role}"
        cmd "touch .lubyanka/site-cookbooks/#{role}/Berksfile"
        cmd "echo \"source 'https://supermarket.chef.io'\" >> .lubyanka/site-cookbooks/#{role}/Berksfile"
        cmd "echo 'metadata' >> .lubyanka/site-cookbooks/#{role}/Berksfile"
        # cmd "ln -s #{dir_path}/metadata.rb .lubyanka/site-cookbooks/#{role}/metadata.rb"
        render_metadata
        render_recipe
        # cmd "ln -s #{dir_path}/recipes .lubyanka/site-cookbooks/#{role}/recipes"
        cmd "echo '{\"run_list\":[\"recipe[#{role}]\"]}' > .lubyanka/nodes/#{role}.json"
      end

      def render_berksfile
        cmd "rm .lubyanka/Berksfile"
        File.write(".lubyanka/Berksfile", Lubyanka::SoftwareManager.cookbook)
      end

      def render_metadata
        File.write(".lubyanka/site-cookbooks/#{role}/metadata.rb", Lubyanka::SoftwareManager.metadata)
      end

      def render_recipe
        cmd "mkdir -p .lubyanka/site-cookbooks/#{role}/recipes"
        File.write(".lubyanka/site-cookbooks/#{role}/recipes/default.rb", Lubyanka::SoftwareManager.recipe)
      end

      def render_databags
        databags = Lubyanka::SoftwareManager.databags.map do |path, json|
          Databag.new(path, json)
        end

        databags.each do |bag|
          cmd "mkdir -p .lubyanka/data_bags/#{File.dirname(bag.path)}"

          File.open(".lubyanka/data_bags/#{bag.path}","w") do |f|
            f.write(bag.pretty_json)
          end
        end
      end

      def knife_local_mode(bool)
        cmd "echo 'local_mode #{bool}' >> .lubyanka/.chef/knife.rb"
      end

      def cmd(command)
        logger.info "Execute cmd: #{command}"
        unless system(command)
          raise "Failed to run command '#{command}'"
          exit 1
        end
      end
    end
  end
end
