module Lubyanka
  module SoftwareManager
    module Extension
      module FuseS3fs
        include Helper

        cookbook 's3fs', git: 'https://github.com/axvm/s3fs-recipe.git'

        def install_s3fs
          add_to_recipe "node.default['s3fs']['data_from_bag'] = true\n"
          add_to_recipe "include_recipe 's3fs'\n"
        end

        def mount_s3fs(id:, buckets:, access_key_id:, secret_access_key:)
          data = {
            id: id,
            buckets: buckets,
            access_key_id: access_key_id,
            secret_access_key: secret_access_key
          }

          set_databag("s3fs/#{id}", data)
          add_to_recipe "node.default['s3fs']['data_bag']['name'] = 's3fs'\n"
          add_to_recipe "node.default['s3fs']['data_bag']['item'] = '#{id}'\n"
          install_s3fs
        end
      end

      register_extension Lubyanka::SoftwareManager::Extension::FuseS3fs
    end
  end
end
