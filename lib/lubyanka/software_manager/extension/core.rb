module Lubyanka
  module SoftwareManager
    module Extension
      module Core
        include Helper

        def file(path, content:, mode: '0600', owner: 'root', group: nil)
          group = owner unless group
          recipe_block('file', path, content: content, mode: mode, owner: owner, group: group)
        end

        def directory(path, owner: 'root', recursive: false, group: nil, mode: '0755', action: :create)
          group = owner unless group
          recipe_block('directory', path, owner: owner, group: group, mode: mode, action: action)
        end

        def bash(name, user: 'root', code: nil)
          recipe_block 'bash', name, user: user, code: code, action: :run
        end

        def install_package(name)
          add_to_recipe("package '#{name}'\n")
        end

        def raw_recipe(string)
          add_to_recipe(string + "\n")
        end

        def raw_add_cookbook(name, version=nil)
          cookbook_record = "cookbook '#{name}'"
          cookbook_record += ", '#{version}'" if version
          cookbook_record += "\n"
          Lubyanka::SoftwareManager.cookbook << cookbook_record
          Lubyanka::SoftwareManager.metadata << "depends '#{name}'\n"
        end

      end

      register_extension Lubyanka::SoftwareManager::Extension::Core
    end
  end
end
