require 'json'

module Lubyanka
  module SoftwareManager
    module Extension
      module Helper

        module ClassMethods
          def cookbook(name, version = nil, git: nil)
            cookbook_record = "cookbook '#{name}'"
            cookbook_record += ", '#{version}'" if version
            cookbook_record += ", git: '#{git}'" if git
            cookbook_record += "\n"
            Lubyanka::SoftwareManager.cookbook << cookbook_record
            Lubyanka::SoftwareManager.metadata << "depends '#{name}'\n"
          end
        end

        def self.included(r)
          r.extend(ClassMethods)
        end

        attr_reader :berksfile, :metadata, :recipe, :databags

        private

        # data - hash or array of hashes
        def add_databag(path, data)
          Lubyanka::SoftwareManager.databags[path] = data
        end

        def databag(path)
          Lubyanka::SoftwareManager.databags[path]
        end

        def set_databag(path, data)
          add_databag(path, data)
        end

        def get_databag(path)
          databag(path)
        end

        def add_to_recipe(cmd)
          Lubyanka::SoftwareManager.recipe << cmd
        end

        def recipe_block(method_name, method_args = [], method_opts = {})
          cmd = if method_args.is_a?(Array)
            "#{method_name} #{parse_values(method_args).join(',')} do"
          else
            "#{method_name} #{parse_value(method_args)} do"
          end.concat("\n")

          opts = method_opts.map do |k, v|
            "  #{k} #{parse_value(v)}"
          end.join("\n").concat("\n")

          cmd << opts << "end\n"

          add_to_recipe(cmd)
        end

        def recipe_method(method_name, method_args = [])
          cmd = if method_args.is_a?(Array)
            "#{method_name}(#{parse_values(method_args).join(',')})"
          else
            "#{method_name}(#{parse_value(method_args)})"
          end.concat("\n")

          add_to_recipe(cmd)
        end

        def parse_value(v)
          if v.is_a?(String)
            "'#{v}'"
          elsif v.is_a?(Symbol)
            ":#{v}"
          elsif v.is_a?(NilClass)
            "nil"
          else
            v
          end
        end

        def parse_values(array)
          array.map do |i|
            parse_value(i)
          end
        end
      end
    end
  end
end
