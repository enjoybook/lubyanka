module Lubyanka
  module SoftwareManager
    module Extension
      module Iptables
        include Helper

        cookbook 'iptables', '~> 4.3.1'

        def iptables_install
          add_to_recipe "include_recipe 'iptables'\n"
        end

        def iptables_open_tcp_port(port_number)
          recipe_block 'iptables_rule', "rule_#{port_number}",
            lines: "-I INPUT -p tcp --dport #{port_number} -m state --state NEW -j ACCEPT"
        end

        def iptables_close_all
          recipe_block 'iptables_rule', "drop_all_input",
            lines: "-P INPUT DROP"
          recipe_block 'iptables_rule', "drop_all_forward",
            lines: "-P FORWARD DROP"
        end
      end

      register_extension Lubyanka::SoftwareManager::Extension::Iptables
    end
  end
end
