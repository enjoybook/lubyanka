module Lubyanka
  module SoftwareManager
    module Extension
      module Rbenv
        include Helper

        cookbook 'ruby_rbenv', '~> 2.0.3'

        def rbenv_install_ruby(version, to: "root", global: false)
          recipe_method("rbenv_user_install", to)
          recipe_block("rbenv_ruby", version, user: to)
          recipe_block("rbenv_global", version, user: to) if global
          rbenv_install_gem("bundler", gem_version: "1.15.4", ruby_version: version, user: to)
        end

        def rbenv_install_gem(gem_name, gem_version:, ruby_version:, user: "root")
          recipe_block('rbenv_gem', gem_name, rbenv_version: ruby_version, version: gem_version, user: user)
        end

      end

      register_extension Lubyanka::SoftwareManager::Extension::Rbenv
    end
  end
end
