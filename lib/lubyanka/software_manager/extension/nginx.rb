module Lubyanka
  module SoftwareManager
    module Extension
      module Nginx
        include Helper

        cookbook 'zypper', '~> 0.4.0'
        cookbook 'chef_nginx', '~> 6.1.1'

        def install_nginx
          add_to_recipe("include_recipe 'chef_nginx::default'\n")
        end

      end

      register_extension Lubyanka::SoftwareManager::Extension::Nginx
    end
  end
end
