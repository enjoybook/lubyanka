module Lubyanka
  module SoftwareManager
    module Extension
      module Nodejs
        include Helper

        cookbook 'yum-epel', '~> 2.1.2'
        cookbook 'nodejs', '~> 4.0.0'

        def install_nodejs(mode = 'binary')
          add_to_recipe "node.default['nodejs']['install_method'] = '#{mode}'\n"
          add_to_recipe "include_recipe 'nodejs'\n"
        end

      end

      register_extension Lubyanka::SoftwareManager::Extension::Nodejs
    end
  end
end
