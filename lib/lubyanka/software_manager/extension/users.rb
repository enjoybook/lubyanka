module Lubyanka
  module SoftwareManager
    module Extension
      module Users
        include Helper

        cookbook 'users', '~> 5.1.0'

        def create_user(name, options)
          opts = {}

          opts[:id] = name

          opts[:password]   = options[:password]        if options[:password]
          opts[:ssh_keys]   = options[:authorized_keys] if options[:authorized_keys]
          opts[:home]       = options[:home_path]       if options[:home_path]
          opts[:uid]        = options[:user_id]         if options[:user_id]
          opts[:shell]      = options[:shell]           if options[:shell]
          if options[:groups]
            opts[:groups] = (options[:groups] << "lubyanka")
          else
            opts[:groups] = [ "lubyanka" ]
          end

          opts[:comment] = "Created via lubyanka"

          add_databag("users/#{name}", opts)
          recipe_init
        end

        def recipe_init
          unless @user_recipe_initialized
            recipe_block('users_manage', "lubyanka", group_id: 10111, action: [:remove, :create], data_bag: 'users')
            @user_recipe_initialized = true
          end
        end
      end

      register_extension Lubyanka::SoftwareManager::Extension::Users
    end
  end
end
