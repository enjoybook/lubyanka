module Lubyanka
  module SoftwareManager
    module Extension
      module Postgresql
        include Helper

        cookbook 'postgresql', '~> 6.1.1'

        # default passwd: gq3h89gq43h98gq3
        def install_postgresql(password = "bd66e670da02f4fee2f0d0f28acd437a")
          add_to_recipe("node.default['postgresql']['password']['postgres'] = '#{password}'\n")
          add_to_recipe("include_recipe 'postgresql::server'\n")
        end

        def create_psql_user(username)
          bash "create-postgres-#{username}-user", user: 'postgres', code: "echo \"CREATE USER #{username} WITH CREATEDB;\" | psql"
        end

        def create_psql_database(db_name, owner: 'postgres')
          bash "create-postgres-#{db_name}-db", user: 'postgres', code: "echo \"CREATE DATABASE #{db_name} OWNER #{owner};\" | psql"
        end
      end

      register_extension Lubyanka::SoftwareManager::Extension::Postgresql
    end
  end
end
