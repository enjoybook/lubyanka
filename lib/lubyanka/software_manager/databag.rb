module Lubyanka
  module SoftwareManager
    class Databag
      attr_reader :path, :payload

      def initialize(path, payload)
        @path = "#{path}.json"
        @payload = payload
      end

      def json
        JSON.dump(@payload)
      end

      def pretty_json
        JSON.pretty_generate(@payload)
      end
    end
  end
end
