module Lubyanka
  module SoftwareManager
    module Extension
      def self.register_extension(klass)
        Lubyanka::SoftwareManager.add_extension(klass)
      end
    end
  end
end
