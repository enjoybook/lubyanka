module Lubyanka
  # Example
  #
  # capistrano do
  #   capfile do
  #     require "something"
  #     install_plugin "something"
  #     raw_code "something"
  #   end
  #
  #   recipe do
  #     set :cvs, :git
  #     set :branch, Lubyanka.fetch(:global_variable)
  #     ...
  #   end
  # end
  class ApplicationManager
    def initialize
    end

    def recipe(&block)
      recipe_object.instance_eval(&block)
    end

    def capfile(&block)
      capfile_object.instance_eval(&block)
    end

    def deploy(server)
      Engine.new(capfile_object, recipe_object).deploy(server)
    end

    private

    def capfile_object
      @capfile ||= Capfile.new
    end

    def recipe_object
      @recipe ||= Recipe.new
    end
  end
end
