require 'git'

module Lubyanka
  class Git
    attr_accessor :pwd

    def initialize(pwd)
      @pwd = pwd
    end

    def current_branch
      current_git_branch
    end

    private

    # TODO: exception handler
    def git_object
      ::Git.open(@pwd)
    end

    # TODO: exception handler
    def current_git_branch
      git_object.current_branch || ""
    end
  end
end
