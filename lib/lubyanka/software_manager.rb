module Lubyanka
  module SoftwareManager
    extend self

    def extensions
      @extensions ||= []
    end

    def add_extension(extension)
      extensions << extension
    end

    def execute(&block)
      Sandbox.execute(&block)
    end

    def run(server)
      Engine.new.configure(server)
    end

    def cookbook
      if @cookbook.nil?
        @cookbook = "source 'https://supermarket.chef.io'\n"
      end

      @cookbook
    end

    def metadata
      if @metadata.nil?
        @metadata = [
          "name 'app'",
          "maintainer 'The Authors'",
          "maintainer_email 'you@example.com'",
          "license 'All Rights Reserved'",
          "description 'Installs/Configures app'",
          "long_description 'Installs/Configures app'",
          "version '0.1.0'",
          "chef_version '>= 12.1' if respond_to?(:chef_version)\n\n"
        ].join("\n")
      end

      @metadata
    end

    def recipe
      @recipe ||= ""
    end

    def databags
      @databags ||= {}
    end

    module Sandbox
      extend self
      attr_reader :initialized
      attr_accessor :force

      def init(extensions = [], force = false)
        return false if @initialized && !force
        extend *extensions
        @initialized = true
      end

      def execute(&block)
        init(Lubyanka::SoftwareManager.extensions, force)
        instance_eval &block
      end
    end
  end
end
