module Lubyanka
  class ApplicationManager
    class Capfile
      def initialize
        require "capistrano/setup"
        require "capistrano/deploy"
        require "capistrano/scm/git"
        install_plugin "Capistrano::SCM::Git"
      end

      def require(path)
        # Kernel.require(path)
        writeln("require \"#{path}\"")
      end

      def install_plugin(klass)
        writeln("install_plugin #{klass}")
      end

      def raw_code(code)
        writeln(code)
      end

      def payload
        @payload ||= ""
      end

      private

      def write(code)
        payload << code
      end

      def writeln(code)
        write("\n")
        write(code)
      end
    end
  end
end
