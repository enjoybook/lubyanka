module Lubyanka
  class ApplicationManager
    class Engine
      include Lubyanka::Helpers::Logger

      attr_reader :role

      def initialize(capfile, recipe)
        @role = :app
        @capfile = capfile
        @recipe = recipe # deploy.rb
      end

      def deploy(server)
        logger.info "Checking capistrano files"

        prepare_directory
        run_deploy_task(server)
        clean_directory
      end

      private

      def run_deploy_task(server)
        cmd "SERVER_IP=#{server.public_ip} cap production deploy"
      end

      def dir_path
        File.join(Dir.pwd, 'config', 'lubyanka')
      end

      def prepare_directory
        render_capfile
        render_deployrb
        cmd "mkdir -p ./config/deploy && echo \"server ENV['SERVER_IP'], user: 'deploy', roles: %w{app db web}\" > ./config/deploy/production.rb"
      end

      def clean_directory
        cmd "rm -f Capfile"
        cmd "rm -f config/deploy.rb"
        cmd "rm -rf config/deploy"
      end

      def render_capfile
        cmd "rm -f Capfile"
        File.write("Capfile", @capfile.payload)
      end

      def render_deployrb
        cmd "rm -f config/deploy.rb"
        File.write("config/deploy.rb", @recipe.payload)
      end

      def cmd(command)
        logger.info "Execute cmd: #{command}"
        unless system(command)
          raise "Failed to run command '#{command}'"
          exit 1
        end
      end
    end
  end
end
