module Lubyanka
  class ApplicationManager
    class Recipe
      def set(variable_name, variable_value)
        value = parse_value(variable_value)
        writeln("set :#{variable_name}, #{value}")
      end

      def fetch(variable_name)
        write("fetch(:#{variable_name})")
      end

      def task(name)
        # TODO
      end

      def payload
        @payload ||= ""
      end

      private

      def parse_value(v)
        if v.is_a?(String)
          "'#{v}'"
        elsif v.is_a?(NilClass)
          "nil"
        else
          v
        end
      end

      def write(code)
        payload << code
      end

      def writeln(code)
        write("\n")
        write(code)
      end
    end
  end
end
