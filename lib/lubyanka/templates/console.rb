Lubyanka.task :console do
  logger.info "Rails console task start"

  invoke_task :prepare

  invoke_task :git_branch
  invoke_task :hardware
  server = fetch(:server)

  logger.info "Connecting via ssh to rails console at #{server.public_ip} as deploy user"

  system "ssh deploy@#{server.public_ip} -t 'PATH=$PATH:$HOME/.rbenv/shims; cd #{fetch(:deploy_path)}/current && RAILS_ENV=#{fetch(:rails_stage)} bundle exec rails console'"
end
