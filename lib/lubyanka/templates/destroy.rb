Lubyanka.task :destroy do
  logger.info "Ssh session task start"

  invoke_task :prepare

  env_branch = ENV['BRANCH']
  unless env_branch
    invoke_task :git_branch
    env_branch = fetch(:branch)
  end

  app_server_name = "#{fetch(:app_name)}-#{env_branch}"
  set :scaleway_organization, ENV['SWAT']
  set :scaleway_token, ENV['SWT']

  server = nil
  hardware Lubyanka::HardwareManager::Providers::Scaleway do
    server = find(app_server_name)
  end

  unless server
    logger.info "Can't find server"
    exit 0
  end

  logger.info "You are executed destroy server command!"
  logger.info "To destroy server please type in console '#{app_server_name}'"

  input = gets.gsub("\n",'')

  unless input == app_server_name
    logger.info "Input mismatch. Aborting..."
    exit 0
  end

  logger.info "Input is correct. Destroying server #{app_server_name}"
  hardware Lubyanka::HardwareManager::Providers::Scaleway do
    destroy(app_server_name)
  end
  logger.info "Server has been destroyed"

end
