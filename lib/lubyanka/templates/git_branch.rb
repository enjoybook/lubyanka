Lubyanka.task :git_branch do
  set :branch, Lubyanka::Git.new(Dir.pwd).current_branch
  git_branch = fetch(:branch)
  if fetch(:branch).nil? || fetch(:branch).empty?
    logger.fatal "Can't get git branch name"
    exit 1
  end
  logger.info "Current branch is #{fetch(:branch)}"
end
