Lubyanka.task :ssh do
  logger.info "Ssh session task start"

  invoke_task :prepare

  invoke_task :git_branch
  invoke_task :hardware
  server = fetch(:server)

  logger.info "Connecting via ssh to #{server.public_ip} as deploy user"
  system "ssh deploy@#{server.public_ip}"
end
