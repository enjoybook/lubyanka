Lubyanka.task :application do
  server = fetch(:server)

  logger.info "Application deploy started"
  capistrano server do
    capfile do
      require "capistrano/rbenv"

      require "capistrano/bundler"
      require "capistrano/rails/assets"
      require "capistrano/rails/migrations"

      require 'capistrano/puma'
      install_plugin 'Capistrano::Puma'
      install_plugin 'Capistrano::Puma::Workers'
    end

    recipe do
      set :application, Lubyanka.fetch(:app_name)
      set :repo_url, Lubyanka.fetch(:git_remote_repo)

      set :stage, Lubyanka.fetch(:rails_stage)
      set :branch, Lubyanka.fetch(:branch)
      # set :branch, 'master'

      set :deploy_to, Lubyanka.fetch(:deploy_path)

      set :linked_dirs, ["log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"]
    end
  end
  logger.info "Deploy finished"
end
