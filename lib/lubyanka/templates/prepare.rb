Lubyanka.task :prepare do
  set :app_name, 'example'
  set :git_remote_repo, 'git@bitbucket.org:enjoybook/dummyapp.git'
  set :rails_stage, 'production'
  set :deploy_path, '/var/www/webapp'
end
