Lubyanka.task :cloudflare do
  set :cloudflare_email, ENV['CF_EMAIL']
  set :cloudflare_api_key, ENV['CF_API_KEY']
  server = fetch(:server)

  logger.info "Updating cloudflare dns record"
  cloudflare = Lubyanka::Cloudflare.new

  if fetch(:branch) == 'master'
    cloudflare.set_a_record(fetch(:domain), 'www', server.public_ip)
    cloudflare.set_a_record(fetch(:domain), '@', server.public_ip)
  else
    cloudflare.set_a_record(fetch(:domain), fetch(:branch), server.public_ip)
    # cloudflare.set_txt_record(fetch(:domain), fetch(:branch), server.ip)
  end

  logger.info "Record updated"
end
