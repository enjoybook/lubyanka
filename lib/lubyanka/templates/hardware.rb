Lubyanka.task :hardware do
  app_server_name = "#{fetch(:app_name)}-#{fetch(:branch)}"
  set :scaleway_organization, ENV['SWAT']
  set :scaleway_token, ENV['SWT']

  server = nil
  hardware Lubyanka::HardwareManager::Providers::Scaleway do
    server = find_or_create(app_server_name)
  end

  set :server, server
end
