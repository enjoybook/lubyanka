Lubyanka.task :software do
  task_dir = File.expand_path(File.dirname(__FILE__))
  server = fetch(:server)
  logger.info "Server: #{server}"

  set :domain, 'example.com'
  if fetch(:branch) == 'master'
    set :nginx_domain, "www.#{fetch(:domain)}"
  else
    set :nginx_domain, "#{fetch(:branch)}.#{fetch(:domain)}"
  end

  nginx_cfg =<<NGINX_CFG
upstream app {
    server unix:/var/www/webapp/shared/sockets/puma.sock fail_timeout=0;
}

server {
    listen 80;
    server_name #{Lubyanka.fetch(:nginx_domain)};

    root #{Lubyanka.fetch(:deploy_path)}current/public;
    client_max_body_size 128M;
    keepalive_timeout 10;

    try_files $uri/index.html $uri @app;

    location @app {
        proxy_pass http://app;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;
    }

    error_page 500 502 503 504 /500.html;
}
NGINX_CFG

  software do
    iptables_close_all
    iptables_open_tcp_port 22 #ssh
    iptables_open_tcp_port 80 #http
    iptables_open_tcp_port 443 #https

    if ENV['S3IAM']
      mount_s3fs id: ENV['S3IAM'], buckets: [ENV['S3BUCKET']], access_key_id: ENV['S3ACCESS'], secret_access_key: ENV['S3SECRET']
    end

    create_user :deploy,
      password: "$1$5cE1rI/9$4p0fomh9U4kAI23qUlZVv/",
      authorized_keys: ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFJeIjHeSYD+ixTyfU4fokAYpTb5ZCkLFJQiCXrWR0U0ZiEStZ1FpEk8p8zj4AkFcZPGXkBJ52Yk8kne78GoBbMvNRadgcWY6k2BLf+9g9UfmRgXCyrVJWUSHnnCyY1RX4/mT+uQ3fTpOITg2s8ZqSavWblAkZPrutf83DIkg8Z313iGagaXtyLLYsLK7iUOkVsZ0sgKrSFVrBONv68q1cl/XB8zd295rvGS2QXsB07/8c4X88tF37muY5rXHXYYSwlxO9PWzNFpVIuVr4hPNEoLt7i41y+S575DTPX8zP5eE2wIfHliuAsGRv7t/NVW5YiRaVEn0yL25tcbEiVtMv axvm@iMac-imac.local"],
      home_path: "/home/deploy",
      groups: [
        "www-data"
      ],
      user_id: 9001,
      shell: "/bin/bash"

    file '/home/deploy/.ssh/id_rsa',
      content: File.read(File.join(task_dir, 'deploy_rsa')),
      mode: '0600',
      owner: 'deploy',
      group: 'www-data'

    file '/home/deploy/.ssh/id_rsa.pub',
      content: File.read(File.join(task_dir, 'deploy_rsa.pub')),
      mode: '0644',
      owner: 'deploy',
      group: 'www-data'

    directory Lubyanka.fetch(:deploy_path),
      owner: 'deploy',
      group: 'www-data'

    rbenv_install_ruby '2.3.3', to: 'deploy', global: true

    install_postgresql
    create_psql_user 'deploy'
    create_psql_database 'webapp', owner: 'deploy'

    install_nginx
    file "/etc/nginx/sites-enabled/webapp.conf", content: nginx_cfg
    install_nodejs
  end
end
