Lubyanka.task :deploy do
  logger.info "Deployment task start"

  invoke_task :prepare

  invoke_task :git_branch
  invoke_task :hardware
  server = fetch(:server)

  logger.info "Installing software"
  invoke_task :software
  software_install server
  logger.info "Software finished"

  invoke_task :application

  invoke_task :cloudflare

  logger.info "Deployment task end"
end
