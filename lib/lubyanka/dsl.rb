require 'active_support/concern'

module Lubyanka
  module DSL
    extend ActiveSupport::Concern #FIXME: remove active support concerns

    included do
      def storage
        @storage ||= {}
      end

      def fetch(key)
        storage[key]
      end

      def set(key, value)
        storage[key] = value
      end

      def hardware(provider, &block)
        Lubyanka::HardwareManager.new(provider).instance_eval(&block)
      end

      def software(&block)
        Lubyanka::SoftwareManager.execute(&block)
      end

      def software_install(server)
        Lubyanka::SoftwareManager.run(server)
      end

      def capistrano(server, &block)
        am = Lubyanka::ApplicationManager.new
        am.instance_eval(&block)
        am.deploy(server)
      end
    end
  end
end
