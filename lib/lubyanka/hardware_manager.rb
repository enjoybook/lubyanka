require 'lubyanka/hardware_manager/server'
require 'lubyanka/hardware_manager/providers/scaleway'

module Lubyanka
  class HardwareManager
    attr_reader :provider

    def initialize(provider)
      @provider = provider
    end

    def find(name)
      @provider.find(name)
    end

    def create(name)
      @provider.create(name)
    end

    def destroy(name)
      @provider.destroy(name)
    end

    def find_or_create(name)
      server = find(name)
      server = create(name) unless server
      server
    end
  end
end
