require 'cloudflair'

module Lubyanka
  class Cloudflare
    def initialize
      Cloudflair.configure do |config|
        config.cloudflare.auth.key = Lubyanka.fetch(:cloudflare_api_key)
        config.cloudflare.auth.email = Lubyanka.fetch(:cloudflare_email)
      end
    end

    def set_a_record(zone_name, subdomain_name, ip, proxy = true)
      zone = find_zone(zone_name)
      raise "Can't find zone #{zone_name} in cloudflare" if zone.nil?

      record = find_dns_record(zone, subdomain_name, 'A')
      unless record.nil?
        record.delete
      end

      unless ip.nil?
        if subdomain_name == '@'
          zone.new_dns_record type: 'A', name: zone_name, content: ip, proxied: proxy
        else
          zone.new_dns_record type: 'A', name: "#{subdomain_name}.#{zone_name}", content: ip, proxied: proxy
        end
      end
    end

    def set_txt_record(zone_name, subdomain_name, content)
      zone = find_zone(zone_name)
      raise "Can't find zone #{zone_name} in cloudflare" if zone.nil?

      record = find_dns_record(zone, subdomain_name, "TXT")
      unless record.nil?
        record.delete
      end

      unless content.nil?
        if subdomain_name == '@'
          zone.new_dns_record type: 'TXT', name: zone_name, content: content
        else
          zone.new_dns_record type: 'TXT', name: "#{subdomain_name}.#{zone_name}", content: content
        end
      end
    end

    private

    def find_dns_record(zone, subdomain, type)
      record_name = subdomain == '@' ? zone.name : "#{subdomain}.#{zone.name}"
      Cloudflair.zone(zone.id).dns_records.find do |record|
        record.name == record_name && record.type == type
      end
    end

    def find_zone(name)
      Cloudflair.zones.find do |zone|
        zone.name == name
      end
    end

    def unwrap
      Cloudflair
    end
  end
end
