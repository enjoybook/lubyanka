require 'scaleway'

module Lubyanka
  class HardwareManager
    module Providers
      module Scaleway
        extend self
        include Lubyanka::Helpers::Logger
        include Lubyanka::Helpers::BlockLoops

        def create(name)
          logger.info "Trying to create server with name '#{name}'"
          server = create_server(name)
          power_on(server.id)

          if find_server_by_name(name).state == "running"
            build_structure(name)
          else
            destroy(name)
            raise "Failed to create server"
          end
        end

        def destroy(name)
          logger.info "Trying to destroy server with name '#{name}'"
          server = find_server_by_name(name)
          terminate(server.id)
        end

        def find(name)
          logger.info "Trying to find server with name '#{name}'"
          find_server_by_name(name).nil? ? nil : build_structure(name)
        end

        private

        def find_server_by_name(name)
          wrapper::Server.all.select do |server|
            server.name == name
          end.first
        end

        def terminate(id)
          wrapper::Server.terminate(id)
        end

        def create_server(name)
          # image = wrapper::Marketplace.find_local_image_by_name('CentOS 7.2', arch: 'x86_64').id
          # wrapper::Server.create(name: name, image: image)
          wrapper::Server.create(name: name)
        end

        def power_on(id)
          3.times do
            logger.info "Trying to launch server"
            wrapper::Server.power_on(id)

            wait_with_timeout(30, 10) do
              break if wrapper::Server.find(id).state == 'running'
              logger.info "Server is not ready yet. Retrying in 30 seconds"
            end

            server = wrapper::Server.find(id)
            if server.state == 'running'
              logger.info "Server successfuly launched at #{server.public_ip.address}"
              break
            else
              logger.info "Failed to start server after 5 mins. Retrying..."
            end
          end
        end

        def wrapper
          unless @initialized
            ::Scaleway.organization = Lubyanka.fetch(:scaleway_organization)
            ::Scaleway.token = Lubyanka.fetch(:scaleway_token)
            ::Scaleway.zone = Lubyanka.fetch(:scaleway_zone)
            @initialized = true
          end

          ::Scaleway
        end

        def build_structure(server_name)
          data = find_server_by_name(server_name)

          struct = Lubyanka::HardwareManager::Server.new
          struct.name = data.name
          struct.state = data.state
          struct.public_ip = data.public_ip.address

          struct
        end
      end
    end
  end
end
