require 'active_support/concern'

module Lubyanka
  module Helpers
    module BlockLoops
      extend ActiveSupport::Concern

      included do
        def wait_with_timeout(timeout_seconds, timeout_iterations)
          raise 'Timout must be greater than 0 seconds' if timeout_seconds < 1
          iterations = 0

          while iterations < timeout_iterations
            yield
            iterations += 1
            sleep(timeout_seconds)
          end
        end
      end
    end
  end
end
