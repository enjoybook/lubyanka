require 'logger'
require 'active_support/all' # TODO: concern and try only used
# require 'active_support/concern'

module Lubyanka
  module Helpers
    module Logger
      extend ActiveSupport::Concern

      included do
        def logger
          @logger ||= ::Logger.new(STDOUT)

          @logger.level =
            case Lubyanka.log_level.try(:to_sym)
            when :debug
              ::Logger::DEBUG
            else
              ::Logger::INFO
            end

          @logger
        end
      end
    end
  end
end
