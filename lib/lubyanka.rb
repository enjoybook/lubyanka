require "lubyanka/version"

require "json"

require 'lubyanka/helpers/logger'
require 'lubyanka/helpers/block_loops'
require 'lubyanka/git'
require 'lubyanka/hardware_manager'
require 'lubyanka/software_manager'
require 'lubyanka/software_manager/engine'
require 'lubyanka/software_manager/databag'
require "lubyanka/software_manager/extension"
require "lubyanka/software_manager/extension/helper"
require "lubyanka/software_manager/extension/core"
require "lubyanka/software_manager/extension/iptables"
require "lubyanka/software_manager/extension/rbenv"
require "lubyanka/software_manager/extension/users"
require "lubyanka/software_manager/extension/postgresql"
require "lubyanka/software_manager/extension/nginx"
require "lubyanka/software_manager/extension/nodejs"
require "lubyanka/software_manager/extension/fuse_s3fs"
require 'lubyanka/application_manager'
require 'lubyanka/application_manager/engine'
require 'lubyanka/application_manager/capfile'
require 'lubyanka/application_manager/recipe'
require 'lubyanka/cloudflare'
require 'lubyanka/dsl'

module Lubyanka
  class << self
    attr_accessor :log_level
    include Lubyanka::Helpers::Logger
    include Lubyanka::DSL

    def exec(args)
      logger.debug "Args: #{args}"
      preload_tasks
      task_name = args.shift || "default"
      @ARGV = args
      invoke_task(task_name)
    end

    def preload_tasks
      tasks_path = File.join(Dir.pwd,'config', 'lubyanka', '*.rb')
      Dir[tasks_path].each do |task_file|
        require task_file
      end
    end

    def task(name, &block)
      tasks[name.to_s] = block
    end

    def invoke_task(name)
      logger.debug "Tasks #{tasks}"
      task = tasks[name.to_s]
      raise "Task #{name} is not defined!" unless task
      logger.debug "Task #{task}"
      instance_eval &task
    end

    def tasks
      @tasks ||= {}
    end

    def arguments
      @ARGV
    end

    def init
      logger.info "Initializing configuration files"
      FileUtils.cp_r File.expand_path('../lubyanka/templates', __FILE__), File.join(Dir.pwd, 'config', 'lubyanka'), verbose: false
      logger.info "Initialization finished. Make changes in tasks and execute 'lubyanka exec taskname' to run task"
    end
  end
end
