Lubyanka

1) Необходимо добавить гем в гемфайл
2) Через bundle exec lubyanka init инициализировать конфиги
3) В database.yml нужно убрать юзера и пароль в продакшн окружении, имя базы изменить на webapp
4) В secrets.yml прописать ключ для продакшн окружения
5) В environments/production.rb включить компиляцию ассетов, откючить использование зашифрованного ключа из secrets.yml
6) Указать домен в nginx.cfg
7) Проверить настройки iptables
8) Настроить CircleCI
9) Радоваться жизни

В CI указать в окружении аксес токены от Scaleway (SWAT - organization, SWT - токек),
в deployment указать bundle exec lubyanka fire
